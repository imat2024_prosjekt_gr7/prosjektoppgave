from functions import *

image_path = get_image_path("image001.png")

f = get_f(image_path)

# a)
# plot_g(1)
# plot_g(0.01)
# plot_g(100)

fgradabs = get_fgradabs(f)
# iii - ulike lambda verdier
plot_g_fgradabs(fgradabs, 1)
# plot_g_fgradabs(fgradabs, 100000000)
# plot_g_fgradabs(fgradabs, 0.1)

dt = 0.2
c = 0.2
lam = 0.5
f_diffused_g = get_f_diffused_g(f, dt, lam, 3)

plot_matrix(f_diffused_g, f"f etter diffusjon med dt = {dt}")

plot_matrix(get_edges(f_diffused_g, c), f"Kanter med c = {c}")