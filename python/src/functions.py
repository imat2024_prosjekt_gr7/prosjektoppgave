# This file contains the functions used in the main files.

# library imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import os

def get_image_path(image:str) -> str:
    """
    Returns the path to the image file.

    Parameters
    ----------
    image : str
        The name of the image file.
    
    Returns
    -------
    str
        The path to the image file.
    """
    path = os.path.dirname(__file__)
    return os.path.join(path, image)

def get_f(image_path:str) -> np.ndarray:
    """
    Returns the image matrix of the image file for one color channel.

    Parameters
    ----------
    image_path : str
        The path to the image file.
    
    Returns
    -------
    np.ndarray
        The matrix of the image file.
    """
    image_matrix = mpimg.imread(image_path)
    return image_matrix[:, :, 0]

def f_x(f:np.ndarray, x:int, y:int, dx:int) -> float:
    """
    Returns the partial derivative of f with respect to x in (x, y).

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    x : int
        The x-coordinate.
    y : int
        The y-coordinate.
    dx : int
        The step size.
    
    Returns
    -------
    float
        The partial derivative of f with respect to x in (x, y).
    """
    return (f[x + dx, y] - f[x - dx, y]) / ( 2* dx)

def f_y(f:np.ndarray, x:int, y:int, dy:int) -> float:
    """
    Returns the partial derivative of f with respect to y in (x, y).

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    x : int
        The x-coordinate.
    y : int
        The y-coordinate.
    dy : int
        The step size.
    
    Returns
    -------
    float
        The partial derivative of f with respect to y in (x, y).
    """
    return (f[x, y + dy] - f[x, y - dy]) / (2 * dy)

def f_xx(f:np.ndarray, x:int, y:int, dx:int) -> float:
    """
    Returns the second partial derivative of f with respect to x in (x, y).

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    x : int
        The x-coordinate.
    y : int
        The y-coordinate.
    dx : int
        The step size.
    
    Returns
    -------
    float
        The second partial derivative of f with respect to x in (x, y).
    """
    # hent ut fra fx i stedet? raskere?
    return (f_x(f, x + dx, y, dx) - f_x(f, x - dx, y, dx))/(2*dx)

def f_yy(f:np.ndarray, x:int, y:int, dy:int) -> float:
    """
    Returns the second partial derivative of f with respect to y in (x, y).

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    x : int
        The x-coordinate.
    y : int
        The y-coordinate.
    dy : int
        The step size.
    
    Returns
    -------
    float
        The second partial derivative of f with respect to y in (x, y).
    """
    return (f_y(f, x, y + dy, dy) - f_y(f, x, y - dy, dy))/(2*dy)

def delta_f(f:np.ndarray, x:int, y:int) -> float:
    """
    Returns the value of delta f in (x, y).
    
    Parameters
    ----------
    f : np.ndarray
        The matrix.
    x : int
        The x-coordinate.
    y : int
        The y-coordinate.
    
    Returns
    -------
    float
        The value of delta f in (x, y).
    """
    dx, dy = 1, 1
    return f_xx(f, x, y, dx) + f_yy(f, x, y, dy)

def nabla_f_abs(fx:np.ndarray, fy:np.ndarray, x:int, y:int) -> float:
    """
    Returns the absolute value of the gradient of f in (x, y).

    Parameters
    ----------
    fx : np.ndarray
        The partial derivative of f with respect to x.
    fy : np.ndarray
        The partial derivative of f with respect to y.
    x : int
        The x-coordinate.
    y : int
        The y-coordinate.
    
    Returns
    -------
    float
        The absolute value of the gradient of f in (x, y).
    """
    return np.sqrt(fx[x, y]**2 + fy[x, y]**2)

def gradient(f:np.ndarray, x:int, y:int) -> float:
    """
    Returns the gradient of f in (x, y).

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    x : int
        The x-coordinate.
    y : int
        The y-coordinate.
    
    Returns
    -------
    float
        The gradient of f in (x, y).
    """
    dx, dy = 1, 1
    return np.sqrt(f_x(f, x, y, dx)**2 + f_y(f, x, y, dy)**2)

def diffusion(f:np.ndarray, x:int, y:int, dt:float) -> float:
    """
    Returns the diffusion of f in (x, y, dt).

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    x : int
        The x-coordinate.
    y : int
        The y-coordinate.
    dt : float
        The time step.
    
    Returns
    -------
    float
        The diffusion of f in (x, y, dt).
    """
    return f[x, y] + dt * delta_f(f, x, y)

def diffusion_g(f:np.ndarray, x:int, y:int, dt:float, lam:float) -> float:
    """
    Returns the diffusion of f in (x, y, dt) using the function g.

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    x : int
        The x-coordinate.
    y : int
        The y-coordinate.
    dt : float
        The time step.
    lam : float
        The value.
    
    Returns
    -------
    float
        The diffusion of f in (x, y, dt) using the function g.
    """
    return f[x, y] + dt * g(gradient(f, x, y), lam) * delta_f(f, x, y)

def fill_difused(f:np.ndarray, dt:float) -> np.ndarray:
    """
    Returns the diffused matrix of f.

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    dt : float
        The time step.
    
    Returns
    -------
    np.ndarray
        The diffused matrix of f.
    """
    for i in range(1, f.shape[0]-2):
        for j in range(1, f.shape[1]-2):
            f[i, j] = diffusion(f, i, j, dt)
    
    return f

def fill_difused_g(f:np.ndarray, dt:float, lam:float) -> np.ndarray:
    """
    Returns the diffused matrix of f using the function g.

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    dt : float
        The time step.
    
    Returns
    -------
    np.ndarray
        The diffused matrix of f using the function g.
    """
    for i in range(1, f.shape[0]-2):
        for j in range(1, f.shape[1]-2):
            f[i, j] = diffusion_g(f, i, j, dt, lam)
    
    return f
    

def fill_array(array:np.ndarray, f:np.ndarray, function, delta:int) -> np.ndarray:
    """
    Returns the array filled with the result of the function.

    Parameters
    ----------
    array : np.ndarray
        The array to fill.
    f : np.ndarray
        The matrix.
    function : function
        The function to use.
    delta : int
        The step size.
    
    Returns
    -------
    np.ndarray
        The array filled with the result of the function.
    """
    for i in range(1, array.shape[0]-2):
        for j in range(1, array.shape[1]-2):
            array[i,j] = function(f, i, j, delta)
    
    return array

def get_fx(f:np.ndarray) -> np.ndarray:
    """
    Returns a matrix filled with the partial derivative of f with respect to x.

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    
    Returns
    -------
    np.ndarray
        A matrix filled with the partial derivative of f with respect to x.
    """
    fx = np.zeros_like(f)
    dx = 1
    return fill_array(fx, f, f_x, dx)

def get_fy(f:np.ndarray) -> np.ndarray:
    """
    Returns a matrix filled with the partial derivative of f with respect to y.

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    
    Returns
    -------
    np.ndarray
        A matrix filled with the partial derivative of f with respect to y.
    """
    fy = np.zeros_like(f)
    dy = 1
    return fill_array(fy, f, f_y, dy)

def get_fgradabs(f:np.ndarray) -> np.ndarray:
    """
    Returns a matrix filled with the absolute values of the gradient of f.

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    
    Returns
    -------
    np.ndarray
        A matrix filled with the absolute values of the gradient of f.
    """
    fgradabs = np.zeros_like(f)
    fx = get_fx(f)
    fy = get_fy(f)
    for i in range(1, fgradabs.shape[0]-1):
        for j in range(1, fgradabs.shape[1]-1):
            fgradabs[i, j] = nabla_f_abs(fx, fy, i, j)
    
    return fgradabs

def norm_matrix(matrix:np.ndarray) -> np.ndarray:
    """
    Returns the normalized matrix.

    Parameters
    ----------
    matrix : np.ndarray
        The matrix.
    
    Returns
    -------
    np.ndarray
        The normalized matrix.
    """
    matrix_min = matrix.min()
    matrix_max = matrix.max()
    return (matrix - matrix_min)/(matrix_max - matrix_min)

def get_norm_fgradabs(fgradabs:np.ndarray) -> np.ndarray:
    """
    Returns a matrix filled with the normalized absolute values of the gradient of f.
    
    Parameters
    ----------
    fgradabs : np.ndarray
        The matrix.
        
    Returns
    -------
    np.ndarray
        A matrix filled with the normalized absolute values of the gradient of f.
    """
    return norm_matrix(fgradabs)

def get_edges(f:np.ndarray, c:float) -> np.ndarray:
    """
    Returns a matrix filled with the edges of f.

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    c : float
        The value to compare with.
    
    Returns
    -------
    np.ndarray
        A matrix filled with the edges of f.
    """
    def cut(x):
        """
        Calculates the edges of a matrix.

        Parameters
        ----------
        x : float
            The value.
        
        Returns
        -------
        int
            0 if x <= c, 1 if x > c.
        """
        return 0 if x <= c else 1
    
    # cut = np.vectorize(cut)
    cut = np.frompyfunc(cut, 1, 1)
    cut = np.vectorize(cut)
    return cut(get_norm_fgradabs(get_fgradabs(f)))

def get_f_diffused(f:np.ndarray, dt:float, iterations:int=1) -> np.ndarray:
    """
    Returns the diffused matrix of f.

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    dt : float
        The time step.
    
    Returns
    -------
    np.ndarray
        The diffused matrix of f.
    """
    for i in range(iterations):
        f = fill_difused(f, dt)
    
    return f

def get_f_diffused_g(f:np.ndarray, dt:float, lam:float, iterations:int=1) -> np.ndarray:
    """
    Returns the diffused matrix of f using the function g.

    Parameters
    ----------
    f : np.ndarray
        The matrix.
    dt : float
        The time step.
    lam : float
        The value.
    
    Returns
    -------
    np.ndarray
        The diffused matrix of f using the function g.
    """
    for i in range(iterations):
        f = fill_difused_g(f, dt, lam)
    
    return f

def g(x:float, lam:float) -> float:
    """
    Represents the function g(x) = 1 / (sqrt(1 + (x^2)/(lam^2))).

    Parameters
    ----------
    x : float
        The value.
    lam : int
        The value.
    
    Returns
    -------
    float
        The value of g(x).
    """
    return 1 / (np.sqrt(1 + (x**2)/(lam**2)))

def plot_matrix(matrix:np.ndarray, title:str) -> None:
    """
    Plots the matrix.

    Parameters
    ----------
    matrix : np.ndarray
        The matrix.
    title : str
        The title of the plot.
    """
    plt.imshow(matrix, cmap='gray')
    plt.colorbar()
    plt.title(title)
    plt.show()

def plot_g(lam:float) -> None:
    """
    Plots the function g(x) = 1 / (sqrt(1 + (x^2)/(lam^2))).

    Parameters
    ----------
    lam : int
        The value.
    """
    x = np.linspace(0, 375, 375)
    y = [g(i, lam) for i in x]
    plt.plot(x, y)
    plt.title(f"g(x) hvor x = [{x[0]}, {x[-1]}] og lambda = {lam}")
    plt.show()

def plot_g_fgradabs(fgradabs:np.ndarray, lam:float) -> None:
    """
    Plots the function g(fgradabs[x, y]).

    Parameters
    ----------
    fgradabs : np.ndarray
        The matrix.
    lam : int
        The value.
    """
    x = fgradabs.reshape(-1)
    x.sort()
    y = [g(i, lam) for i in x]
    plt.plot(x, y)
    plt.title(f"g(fgradabs) hvor lambda = {lam}")
    plt.xlabel("fgradabs")
    plt.ylabel("g(fgradabs)")
    plt.show()