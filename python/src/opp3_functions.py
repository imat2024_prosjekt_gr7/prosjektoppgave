from functions import *

image_path = get_image_path("image001.png")

f = get_f(image_path)

dt = 0.2
c = 0.2
f_diffused = get_f_diffused(f, dt, 50)

plot_matrix(get_fgradabs(f), "fgradabs")
plot_matrix(get_fgradabs(f_diffused), "fgradabs etter diffusjon")

plot_matrix(f_diffused, f"f etter diffusjon med dt = {dt}")  # a)
plot_matrix(get_edges(f_diffused, c), f"Kanter med c = {c}")    # c)
plot_matrix(get_edges(f_diffused, 0.1), f"Kanter med c = {0.1}")    # c)
plot_matrix(get_edges(f_diffused, 0.15), f"Kanter med c = {0.15}")    # c)

# dt = 0.5
# c = 0.2
# f_diffused = get_f_diffused(f, dt, 2)

# plot_matrix(f_diffused, f"f etter diffusjon med dt = {dt}")  # a)
# plot_matrix(get_edges(f_diffused, c), f"Kanter med c = {c}")    # c)