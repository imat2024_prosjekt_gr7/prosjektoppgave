import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

# Laster inn bildet
image_path = "python/src/image001.png"

# Leser inn bildet
image = mpimg.imread(image_path)

# Tar bare ut en av fargekanalene
f = image[:, :, 0]

# Definerer delta x og delta y

delta_x = 1
delta_y = 1

# Lager to matriser som skal holde på de deriverte verdiene
f_x = np.zeros_like(f)
f_y = np.zeros_like(f)

# Definerer funksjonen fx
def fx(f,x,y, dx):
    return (f[x+dx,y] - f[x-dx,y])/(2*dx)

# Definerer funksjonen fy
def fy(f,x,y, dy):
    return (f[x,y+dy] - f[x,y-dy])/(2*dy)

# Lager en matrise som skal holde på den totale gradienten
fgradabs = np.zeros((f.shape[0], f.shape[1]))

# Fyller inn matrisene med gradienten og de deriverte verdiene
for i in range(1, f.shape[0] - 1):  
    for j in range(1, f.shape[1] - 1):  
        f_x[i, j] = fx(f, i, j, delta_x)
        f_y[i, j] = fy(f, i, j, delta_y)
        fgradabs[i,j] = np.sqrt(f_x[i, j]**2 + f_y[i, j]**2)


# Skalerer matrisa slik at min og maks verdiene er henholdsvis 0 og 1 og alle 
# andre verdier er proporsjonale til dette
fgradabs_norm = (fgradabs-np.min(fgradabs))/(np.max(fgradabs)-np.min(fgradabs))

# Lager en matrise som skal holde på kantene
edges = np.zeros_like(fgradabs_norm)

# Definerer en konstant som bestemmer sensitivitet til kantene
c = 0.2

# Funksjonen som skal brukes til å lage kantene, gjør om alle verdier 
# under c til 0 og resten til 1 hvor sensitiv den skal være med hva 
# som er/ikke er en kant 
def cut(x):
    if x > c:
        return 0
    else:
        return 1

# Vectorizer funksjonen slik at den kan brukes på hele matrisa
vectorized_cut = np.vectorize(cut)

# Lager kantene
edges = vectorized_cut(fgradabs_norm)

# Plotter bildene
def plot_cut_edges():
    plt.imshow(edges, cmap='gray')
    plt.colorbar()
    plt.title('Cut edges from gradient')
    plt.show()

def plot_fgradabs_norm():
    plt.imshow(fgradabs_norm, cmap='gray')
    plt.colorbar()
    plt.title('Normalized gradient')
    plt.show()

def plot_fgradabs():
    plt.imshow(fgradabs, cmap='gray')
    plt.colorbar()
    plt.title('Gradient')
    plt.show()

def plot_f():
    plt.imshow(f, cmap='gray')
    plt.colorbar()
    plt.title('Original image')
    plt.show()

plot_cut_edges()
plot_fgradabs_norm()
plot_fgradabs()
plot_f()