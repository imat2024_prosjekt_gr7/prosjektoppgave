from functions import *

image_path = get_image_path("image001.png")

f = get_f(image_path)
fgradabs = get_fgradabs(f)

plot_matrix(get_fx(f), "fx")   # a)
plot_matrix(get_fy(f), "fy")   # b)
plot_matrix(fgradabs, "Gradient f")     # c)
plot_matrix(get_norm_fgradabs(fgradabs), "Normalisert gradient f")  # d)
plot_matrix(get_edges(f, 0.2), "Kanter")    # g)